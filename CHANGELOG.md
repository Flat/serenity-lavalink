# 0.0.3-alpha
- Added easy queues
- Added nodes

# 0.0.2-alpha
- Added start time to play()
- Added finish time to play()
- Added overwrite current stream to play()
- Added pause()
- Added resume()
- Added stop()
- Added destroy()
- Added seek()
- Added set_volume()
- Updated serenity.

# 0.0.1-alpha
- Initial release
