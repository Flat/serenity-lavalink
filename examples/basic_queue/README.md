### How to run
This example is made to work with the default configuration for lavalink.
/
If you would like to change the configuration on your own, you can do so by modifying the attributes on the lava_client before it gets inserted to data.

Then it's just as easily as just `cargo run --release`

To be consistent with serenity's examples, `~` is the default prefix.
